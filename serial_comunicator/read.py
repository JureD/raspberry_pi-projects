import sqlite3
import matplotlib.pyplot as plt

conn = sqlite3.connect('lighting.db')
c = conn.cursor()
lines = c.execute(''' SELECT * FROM readings WHERE date=?''', ('26-03-2019',))

time = []
light0 = []
light1 = []



for i in lines:
    time.append(i[2])
    light0.append((i[3] + i[4] + i[5]) / 3)
    light1.append((i[6] + i[7]) / 2)
    print(i)

plt.plot(time, light0, label='spodaj')
plt.plot(time, light1, label='zgoraj')
plt.legend()
plt.show()