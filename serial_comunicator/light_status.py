import sqlite3
import time
import communicator as comm

conection_to_arduino = comm.communicator()
values = conection_to_arduino.get_reading().rstrip().split('-')

conn = sqlite3.connect('lighting.db')
c = conn.cursor()

#c.execute( CREATE TABLE IF NOT EXISTS readings (
#                                id integer PRIMARY KEY,
#                                date text NOT NULL,
#                                time real NOT NULL,
#                                sensor0 integer NOT NULL,
#                                sensor1 integer NOT NULL,
#                                sensor2 integer NOT NULL,
#                                sensor3 integer NOT NULL,
#                                sensor4 integer NOT NULL); )

current_date = time.strftime('%d-%m-%Y')
current_hour = int(time.strftime('%H'))
current_minute = float(time.strftime('%M')) / 60
current_time = current_hour + current_minute

write_tuple = (current_date, current_time, int(values[0]), int(values[1]), int(values[2]), int(values[3]), int(values[4]))

c.execute(''' INSERT INTO readings(date,time,sensor0,sensor1,sensor2,sensor3,sensor4)
                        VALUES(?,?,?,?,?,?,?)''', write_tuple)


conn.commit()
conn.close()

