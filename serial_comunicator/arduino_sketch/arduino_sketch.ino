String inputString = "";
bool stringComplete = false;

int red_led = 7;
int green_led = 8;
int status_led = 11;

void setup() {
  Serial.begin(9600);
  inputString.reserve(200);
  pinMode(red_led, OUTPUT);
  pinMode(green_led, OUTPUT);
  pinMode(status_led, OUTPUT);
    
}

void loop() {
  if (stringComplete) {
    if (inputString[0] == '0') {
      if (inputString[1] == '0') {
        digitalWrite(green_led, HIGH);
        digitalWrite(red_led, LOW);
      }
      else if (inputString[1] == '1') {
        digitalWrite(green_led, LOW);
        digitalWrite(red_led, HIGH);
      }
    }
    else if (inputString[0] == '1') {
      delay(1000);
      int value0 = analogRead(A0);
      int value1 = analogRead(A1);
      int value2 = analogRead(A2);
      int value3 = analogRead(A3);
      int value4 = analogRead(A4);
      Serial.print(value0);
      Serial.print("-");
      Serial.print(value1);
      Serial.print("-");
      Serial.print(value2);
      Serial.print("-");
      Serial.print(value3);
      Serial.print("-");
      Serial.println(value4);
    }
    if (inputString[0] == '2') {
      if (inputString[1] == '0') {
        digitalWrite(status_led, HIGH);
      }
      else if (inputString[1] == '1') {
        digitalWrite(status_led, LOW);
      }
    }
    inputString = "";
    stringComplete = false;
  }
}

void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag so the main loop can
    // do something about it:
    if (inChar == '\n') {
      stringComplete = true;
    }
  }
}
