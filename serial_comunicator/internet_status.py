import communicator as comm
import socket

def check_internet():
    try:
        socket.create_connection(('www.google.com', 80))
        return True
    except OSError:
        pass
    return False

status = check_internet()
conn = comm.communicator()

if status == True:
    conn.connection_on()
else:
    conn.connection_off()

