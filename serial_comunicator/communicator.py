import serial
from time import sleep

class communicator:
    def __init__(self, tty = '/dev/ttyUSB0'):
        self.ser = serial.Serial(tty, 9600)

    def get_reading(self):
        sleep(2)
        self.ser.write(b'1\n')
        values = self.ser.readline()
        return values.decode("utf-8")

    def connection_on(self):
        sleep(2)
        self.ser.write(b'00\n')

    def connection_off(self):
        sleep(2)
        self.ser.write(b'01\n')
    
    def status_on(self):
        sleep(2)
        self.ser.write(b'20\n')

    def status_off(self):
        sleep(2)
        self.ser.write(b'21\n')
