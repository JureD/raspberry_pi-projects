#!/usr/bin/env python3

import argparse
import sys
import time
import sqlite3
import matplotlib.pyplot as plt

def show():
    conn = sqlite3.connect('lighting.db')
    c = conn.cursor()
    lines = c.execute(''' SELECT * FROM readings''')

    for i in lines:
        print(i)

def show_date(date):
    conn = sqlite3.connect('lighting.db')
    c = conn.cursor()
    lines = c.execute(''' SELECT * FROM readings WHERE date=?''', (date,))

    for i in lines:
        print(i)

def plot_date(date):
    conn = sqlite3.connect('lighting.db')
    c = conn.cursor()
    lines = c.execute(''' SELECT * FROM readings WHERE date=?''', (date,))

    time = []
    light0 = []
    light1 = []

    for i in lines:
        time.append(i[2])
        light0.append((i[3] + i[4] + i[5]) / 3)
        light1.append((i[6] + i[7]) / 2)

    plt.plot(time, light0, label='spodaj')
    plt.plot(time, light1, label='zgoraj')
    plt.legend()
    plt.show()

def plot_all():
    conn = sqlite3.connect('lighting.db')
    c = conn.cursor()
    lines = c.execute(''' SELECT * FROM readings''')

    dates = set([])

    for i in lines:
        dates.add(i[1])


    for i in dates:
        lines = c.execute(''' SELECT * FROM readings WHERE date=?''', (i,))
        light_values = []
        time_stamps = []
        for j in lines:
            light_values.append((j[3] + j[4] + j[5] + j[6] + j[7]) / 5)
            time_stamps.append(j[2])
        plt.plot(time_stamps, light_values, label=i)

    plt.legend()
    plt.show()

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', action='store_true', default=False, help='Show database entries.')
    parser.add_argument('-S', nargs='?', const=time.strftime('%d-%m-%Y'), type=str, help='Show database entries from date. [dd-mm-YYYY]')
    parser.add_argument('-P', nargs='?', const=time.strftime('%d-%m-%Y'), type=str, help='Plot database entries from date.')
    parser.add_argument('-PA', action='store_true', default=False, help='Plot all days from database.')
    args, leftovers = parser.parse_known_args()

    if args.s == True:
        show()
    elif args.S is not None:
        show_date(args.S)
    elif args.P is not None:
        plot_date(args.P)
    elif args.PA == True:
        plot_all()


if __name__ == '__main__':
    main()

